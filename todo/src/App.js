import React, { Component } from 'react';

import Navbar from './components/navbar/navbar';

import { connect } from 'react-redux';

class App extends Component {

  state = {
    clients: [
      { firstName: 'Josh', lastName: 'Winters', age: 42, id: 1 },
      { firstName: 'Wendy', lastName: 'Winters', age: 41, id: 2 },
      { firstName: 'Lily', lastName: 'Wilson', age: 13, id: 3 }

    ]
  }

  render() {

    console.log(this.props);
    return (
      <div className="App">
        <Navbar clients={this.props.clients} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    clients: state.clients
  }
}

export default connect(mapStateToProps)(App);
