

const initState = {
    clients: [
        { firstName: 'Josh', lastName: 'Winters', age: 42, id: 1 }
    ]
}

const rootReducer = (state = initState, action) => {
    return state;
}

export default rootReducer;