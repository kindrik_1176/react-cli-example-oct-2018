



import React from 'react';

// Example of destructured props in a functional component
const Navbar = ({ clients }) => {

    const clientList = clients.map(client => {
        return (
            <div key={client.id}>
                <p>First Name: {client.firstName}</p>
                <p>Last Name: {client.lastName}</p>
                <p>Age: {client.age}</p>
            </div>
        )
    })

    return (
        <div>
            <h2>Client List</h2>
            {clientList}
        </div>
    )
}

export default Navbar;